class Check                                                                          //Class containing static method to compare objects of class
{
      public static boolean compare(Object FirstObj, Object SecondObj)              
      {
       	  boolean flag = false;
    
           if (SecondObj == null || FirstObj == null )                                 //Check whether any of the objects are null
           {
              flag = false;
           }
		  
	       if((SecondObj.getClass().equals(FirstObj.getClass())) )                       
	       {
	         flag = true;
	       }
         return flag;
	   
      }
}

class First                                                                   //Parent class of class named Second
{
   int x = 10;
   void delta()
   {
     System.out.println(x);
     System.out.println("Base class of First");
   }
}
class Second extends First                                                      //class named Second extending class First
{
    int x;
	String y, z;
	Second()   //Zero parameter constructor
	{
	}
	
	Second(int x, String y, String z) //Parameterized constructor
	{
	    this.x = x;
		this.y = y;
		this.z = z;
	} 
	 @Override
    public boolean equals(Object Obj)                                          //Overriding equals method for class Second
   { boolean flag = false;
    
       if (Obj == null || getClass() != Obj.getClass()) 
       {
          flag = false;
       }
	 
	   if(!(Obj instanceof Second))                              //checking whether the object is  instance of class named Second or not
	   {
	       flag = false;
	   }  
	   Second A = (Second) Obj;
	   
	   if(x == A.x && y.equals(A.y) && z.equals(A.z))             //Checking whether the attributes of two objects are equal or not
	   {
           flag = true;
       }
		
       return flag;
	   
   }
}

class Next                                                          //Some random class just to check what output we get if try to compare its object with some other class
{
  int x=5, y=10;
}
public class Comparison
{
   public static void main(String [] args)
   {
        try{
		First B = new Second(2, "aa", "bb");
        Second F = new Second(1,"java","assignment");
		Second T = new Second(1,"java","assignment");
	 
		Next N = new Next();
		if(Check.compare(F,B))
		{
		  if(F.equals(B))
		   {
		     System.out.println("Same class and state ");
	       }
		   else
		  {
		     System.out.println("Different state");
		  }
		}
		else
		System.out.println("Different classes");
		
		}
		catch(ClassCastException c)
		{
		   System.out.println(c);
		}
		catch(NullPointerException n)
		{
		   System.out.println(n);
		}
		catch(Exception e)
		{
		  System.out.println(e);
		} 
	}
}